<?php

// 1. koneksi db
$dsn = "mysql:host=localhost;dbname=belajarphp";
$kunci = new PDO ($dsn, 'rahmi2', 'Rahmi123!@#');

// 2. Query SQL
$sql = "SELECT * FROM prodi
		WHERE id = ?";

// 3. Exsekusi SQL
$hasil = $kunci->prepare($sql);
$hasil->execute([$_GET['id']]);

// 4. menampilkan hasil Query
$data = $hasil->fetch();
?>

<h1>Edit Program Studi</h1>
<form action="edit_proses.php" method="post">
	Kode:
	<input type="text" name="kode" value="<?= $data['kode']; ?>" /><br />
	Nama:
	<input type="text" name="nama" value="<?= $data['nama']; ?>" /><br />
	<input type="hidden" name="id" value="<?= $data['id']; ?>" />
	<button type="submit">Simpan Perubahan</button>
</form>