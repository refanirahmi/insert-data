<?php // /process/prodi.php

require '../function.php';
$pdo = koneksiDB();

// INSERT/ADD
if($_GET['action'] == "add"){
	$sql = "INSERT INTO prodi (kode, nama)
		VALUES (?, ?)";
	$stmt = $pdo->prepare($sql);
	$stmt->execute([$_GET['kode'], $_GET['nama']]);
	header('Location: ../index.php?page=prodi');
 }elseif($_GET['action'] == "edit"){
 	$sql = "UPDATE prodi
 			SET kode = ?,
 				nama = ?
 			WHERE id = ?";
 	$stmt = $pdo->prepare($sql);
 	$stmt->execute([
 		$_POST['kode'],
 		$_POST['nama'],
 		$_POST['id']
 	]);

 	header('Location: ../index.php?page=prodi');
 }elseif($_GET['action'] == "delete"){
 	$sql = "DELETE FROM prodi
 			WHERE id = ?";
 	$stmt = $pdo->prepare($sql);
 	$stmt->execute([$_GET['id']]);
 	header('Location: ../index.php?page=prodi');
 }
