<?php

$action = $_GET['action'];
require '../function.php';

// 1. koneksi DB
$pdo = koneksiDB();

if($action == "add"){
	// INSERT DATA
	// 2. SQL
	$sql = "INSERT INTO mahasiswa(nim, nama, tanggal_lahir, jenis_kelamin, foto, prodi_id)
		VALUES (?, ?, ?, ?, ?, ?)";

		// 3. prepare dan execute
		$stmt = $pdo->prepare($sql);
		$ext_boleh = ["jpg", "png", "jpeg"];
		if(checkFile($_FILES['foto'], $ext_boleh)){

			$ext = getFileExt($_FILES['foto']);
			$temp = $_FILES['foto']['tmp_name'];
			$permanent_path = "../upload/" . $_POST['nim'] . "." . $ext;
			$file_path = "upload/" . $_POST['nim'] . "." . $ext;

			if(!file_exist("../uploads")){
				mkdir("../uploads");
			}

			move_uploaded_file($temp, $permanent_path);

		$data = [
			$_POST['nim'],
			$_POST['nama'],
			$_POST['tgl_lahir'],
			$_POST['jk'],
			$file_path,
			$_POST['prodi'],
		];

	}else{
			$data = [
			$_POST['nim'],
			$_POST['nama'],
			$_POST['tgl_lahir'],
			$_POST['jk'],
			null,
			$_POST['prodi'],
		];
	}
	$stmt->execute($data);

	header('Location: ../index.php?page=mahasiswa');
}