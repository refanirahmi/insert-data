<?php
// 1. KOneksi Db / Kunci Gudang

	$dsn = "mysql:host=localhost;dbname=belajarphp"; 
	$kunci = new PDO ($dsn, "rahmi2", "Rahmi123!@#");

	// 2. query Sql / surat perintah kerja 
	$sql = "SELECT * FROM prodi";

		
	// 3. eksekusi sql (menjalankan sql)
	$hasil = $kunci->prepare($sql);
	$hasil->execute();

	// 4. menampilkan hasil query (khusus select)
	?>
	<table border="1">
	<tr>
		<th>No</th>
		<th>Kode</th>
		<th>Nama Prodi</th>
		<th>Tindakan</th>
	</tr>

	<?php 
	$i = 0;
	while($data = $hasil->fetch()): 
		$i++; ?>
	<tr>
		<td><?= $i ?></td>
		<td><?= $data['kode']; ?></td>
		<td><?= $data['nama']; ?></td>
		<td>
			<a href="edit.php?id=<?= $data['id']; ?>">Ubah</a>
			<a href="delete.php?id=<?= $data['id']; ?>">Hapus</a>
		</td>
	</tr>
	<?php endwhile; ?>

</table>

