<?php 
$action = $_GET['action'];
if($action == 'add'){
	$page_title = "Buat Mahasiswa Baru";
}

require 'function.php';
// 1. koneksi DB
$pdo = koneksiDB();

// 2. SQL
$sql = "SELECT * FROM prodi";

// 3. prepare dan execute
$hasil = $pdo->query($sql);

?>

<h1 class="h2 mt-3"><?= $page_title; ?></h1>
<form action="process/mahasiswa.php?action=<?= $action ?>"
	  method="post"
	  enctype="multipart/form-data">

<div class="form-group">
	<label>NIM</label>
	<input type="text" name="nim" class="form-control" />
</div>
<div class="form-group">
	<label>Nama</label>
	<input type="text" name="nama" class="form-control" />
</div>
<div class="form-group">
	<label>tanggal Lahir</label>
	<input type="text" name="tgl_lahir" class="form-control" placeholder="YYY-MM-DD" />
</div>
<div class="form-group">
	<label>Jenis Kelamin</label>
	<label><input type="radio" name="jk" value="M" />Laki-Laki</label>
	<label><input type="radio" name="jk" value="F" />Perempuan</label>
</div>
<div class="form-group">
	<label>Foto</label>
	<input type="file" name="foto"  />
</div>
<div class="form-group">
	<label>Program Studi</label>
	<select name="prodi" class="form-control">
	<?php while($prodi = $hasil->fetch()); ?>
		<option value="<?= $prodi['id']; ?>"><?= $prodi['nama']; ?></option>
	<? endwhile; ?>
	</select>
</div>
<button type="submit" class="btn btn-success">Simpan</button>

</form>