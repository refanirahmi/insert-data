<?php

// 1. koneksi DB
require 'function.php';
$pdo = koneksiDB();

// 2. SQL
$sql = "SELECT 
			mahasiswa.id,
			mahasiswa.nim,
			mahasiswa.nama AS nama_mhs,
			mahasiswa.foto,
			prodi.nama AS nama_prodi
		FROM mahasiswa
		JOIN prodi
		ON mahasiswa.prodi_id = prodi.id";

// 3. prepare & execute
$hasil = $pdo->query($sql);

?>
<h1 class="h2 mt-3">Mahasiswa</h1>
<a href="index.php?page=mahasiswa-form&action=add" class="btn btn-primary">
<span data-feather="plus-circle"></span> Buat Mahasiswa Baru</a>

<div class="table-responsive mt-3">
	<table class="table">
		<tr>
			<th>No.</th>
			<th>Foto</th>
			<th>NIM</th>
			<th>Nama</th>
			<th>Program Studi</th>
			<th>Tindakan</th>
		</tr>

		<?php
		$i = 0;
		while($row = $hasil->fetch()):
		$i++;
		?>
		<tr>
			<td><?= $i; ?></td>
			<td>
				<?php if(file_exists("uploads/" . $row['nim'] . "jpg")): ?>
				<img src="<?= $row['foto'] ?>" />
			<?php endif; ?>

			</td>
			<td><?= $row['nim']; ?></td>
			<td><?= $row['nama_mhs']; ?></td>
			<td><?= $row['nama_prodi']; ?></td>
			<td>
				<a href="" class="btn btn-sm btn-primary">
					<span data-feather="eye"></span> Lihat</a>
				<a href="" class="btn btn-sm btn-warning">
					<span data-feather="edit"></span> Ubah</a>
				<a href="" class="btn btn-sm btn-danger">
					<span data-feather="trash-2"></span>  Hapus</a>

			</td>
		</tr>
	<?php endwhile; ?>
	</table>
</div>

